<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'prueba-wordpress-ahenriquez_ej1' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '12345' );

/** MySQL hostname */
define( 'DB_HOST', '127.0.0.1' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Fh6+J_{:/Z,SHhDdxvhfK C.Els&od3MrZ~D?3sdwP&BTEnuL^!Q^f^O0o>/,Mgx' );
define( 'SECURE_AUTH_KEY',  's|eXyF;7$nQ~Xsh*(^JCY;k#xP<U>8/IjTm007{Zi9]ihWy]$3ik@a-Y*W*P$`5e' );
define( 'LOGGED_IN_KEY',    '(_58d|H)l9/FB}v!cb3R1QUs1liOVXcu2(nPrRL$i>0[]j-V[P3rOqt6]DoGZT0$' );
define( 'NONCE_KEY',        'n~NQ}ub7]qh`lP6Q=*Gp`,qG4{H-g@hK@)_3?,On[:UWvW*pW~5mD6K<Ys|*(3Ec' );
define( 'AUTH_SALT',        '^DkJ|q!FOIv42F2D_$-%q>JLZ27blbm+9nrRWWa,96#M?;5kEWYG8iMSsjQul3Qy' );
define( 'SECURE_AUTH_SALT', '9:E`s?^9N<<q{ @B^b7X QuXB1yNGg-yN)l9rv|=s9O83M|sdEDWLo:,!{> %]pX' );
define( 'LOGGED_IN_SALT',   '0Z-Hc~#pNQ!;x-6`:hKE=4Coq@Z|9&zb0AGkc)F`Zcc~<=g{s3CQZcenVXc|p8m}' );
define( 'NONCE_SALT',       'U);J >b1F8u4kZ6J!f*=*V0 s`*r9{^BW[#S$Sae.SE^QInrP[Op^4=EmYYj;Q3>' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}



/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

define('WP_ALLOW_REPAIR',true);
