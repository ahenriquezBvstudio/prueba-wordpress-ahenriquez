<?php wp_head(); ?>

<?php 
  $home = get_page_link(get_page_by_title("Home"));
  $about = get_page_link(get_page_by_title("About" ));
  $news = get_page_link(get_page_by_title("news" ));

?>

<nav class="navbar navbar-expand-lg  navbar-dark bg-dark mb-4" role="navigation">
  <a class="navbar-brand" href="/">Restaurant test</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
    aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarText">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item">
        <a class="nav-link" href="<?php echo $home;?>">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo $about;?>">About us</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="modal" data-target="#exampleModal">Contact</a>
      </li>

      <li class="nav-item">
        <a class="nav-link" href="<?php echo $news;?>">News</a>
      </li>

    </ul>

  </div>

</nav>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Contact </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <input type="hidden" id="contact_nonce" name="contact_nonce" value="<?php echo wp_create_nonce('contact_nonce');?>" />
        <form id="form_contact">

          <div class="form-group">
            <label for="first_name">First name</label>
            <input type="text" class="form-control" id="first_name" name="first_name" placeholder="First Name">
            <span  style="color:red" id="formErrorFirstName"></span>
          </div>

          <div class="form-group">
            <label for="last_name">Last name</label>
            <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last Name">
          </div>
          

          <div class="form-group">
            <label for="email">Email address</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Email">
            <span style="color:red" id="formErrorEmail"></span>
          </div>


          <div class="form-group">
            <label for="exampleInputEmail1">Message </label>
            <textarea class="form-control" id="message" name="message" placeholder="message" rows="3"></textarea>
          </div>
        
      
         
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button id="btn-submit"  type="submit" class="btn btn-primary">Submit</button>
      </div>
    </div>
  </div>
</div>