<?php
/**
* Template Name: Test theme
*
* @package WordPress
* @subpackage First Exercise Acf
*/
get_header();
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>test template</title>

</head>

<body>

    <section>

        <div class="container">

            <h2 style="color:#212529"><?php the_field('Title'); ?></h2>

            <?php 

$images = get_field('gallery');
$size = 'full'; // (thumbnail, medium, large, full or custom size)

if( $images ): ?>
            <div class="row">
                <?php foreach( $images as $image ): ?>
                <div class="d-inline p-3 ">

                    <a href="<?php echo $image['url']; ?>">
                        <img src="<?php echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </a>
                    <p><?php echo $image['caption']; ?></p>
                </div>

                <?php endforeach; ?>

                <?php endif; ?>

            </div>
        </div>

        <div class="container">
            <div class="row  text-justify justify-content-md-center">
               
                <h2 style="color:#212529"><?php the_field('description'); ?></h2>
                <div class="col-mx-auto">

                    <?php the_field('textarea'); ?>
                </div>

            </div>
        </div>
        <br>


    <div class="container">

        
        <h2 style="color:#212529"><?php the_field('comments'); ?></h2>

        
                
            <?php 
                
                if( have_rows('post') ): ?>

                <?php while( have_rows('post') ): the_row(); ?>

            
                    <div class="row">
                        <div class="col-sm-4">

                        <?php
                            
                            $image = get_sub_field('imagen');
                            if( $image ):

                            // Image variables.
                            $url = $image['url'];
                            $title = $image['title'];
                            $alt = $image['alt'];
                            $caption = $image['caption'];

                            // Thumbnail size attributes.
                            $size = 'medium';
                            $thumb = $image['sizes'][ $size ];
                            $width = $image['sizes'][ $size . '-width' ];
                            $height = $image['sizes'][ $size . '-height' ];

                            // Begin caption wrap.
                            if( $caption ): ?>
                                <div class="wp-caption">
                                    <?php endif; ?>

                                    <img src="<?php echo esc_url($thumb); ?>" class="card-img-top" alt="<?php echo esc_attr($alt); ?>" />
                                </div>

                        <?php endif; ?>
                            

                        </div>
                        
                        <div class="col-sm-8">
                            <h4> <?php the_sub_field('text'); ?></h4> 

                                <p class="text-justify"><?php the_sub_field('comment'); ?></p>
                        </div>
                    </div>
                    <br>
                        <?php endwhile; ?>
                        <?php endif; ?>

        </div>




    </section>

</body>

</html>