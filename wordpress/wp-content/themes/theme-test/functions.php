
<?php

// function wpdocs_theme_name_scripts() {
  
//    // wp_enqueue_script( 'script-name', get_template_directory_uri() . '/js/example.js', array(), '1.0.0', true );
// }
// add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );



function bootstrap_cdn() {
 wp_enqueue_style( "Bootstrap-style-min", get_template_directory_uri().'/css/bootstrap.min.css', array(), null, "all" );
 wp_enqueue_script( "Bootstrap-bundle-js-min", get_template_directory_uri().'/js/bootstrap.min.js', array('jquery'), null, false);

 wp_enqueue_script( "contact", get_template_directory_uri().'/js/contact.js', array('jquery'), null, false);
 wp_enqueue_script( "register", get_template_directory_uri().'/js/registerValid.js', array('jquery'), null, false);

 wp_enqueue_script( "sweet2", 'https://cdn.jsdelivr.net/npm/sweetalert2@9', array('jquery'), null, false);


 wp_localize_script( "contact", "contact_vars",['ajaxurl' => admin_url('admin-ajax.php')]);
}

add_action( 'wp_enqueue_scripts', 'bootstrap_cdn' );

add_action( 'wp_ajax_contact_form', 'contact_form' );
add_action( 'wp_ajax_nopriv_contact_form', 'contact_form' );

function contact_form(){

  
    echo "Conectado";

  

    if(!check_ajax_referer("contact_nonce","nonce")){
        return wp_send_json_error(["error"=> "error in nonce"], 400);
    }

    $first_name=  sanitize_text_field( $_POST["first_name"] );   
    $last_name =  sanitize_text_field( $_POST["last_name"]); 
    $email =      sanitize_email( $_POST["email"]);  
    $message =    sanitize_textarea_field(  $_POST["message"]);  

  
    $data = array(

        "post_title" => $first_name,
        "post_type"=>"contacts",
        "post_status"=> "private",
        "post_name"=> $first_name,
        "post_author"=> get_current_user(),
        "meta_input" => array(

            "first_name" => $first_name,
            "last_name" => $last_name,
            "email" => $email,
            "message"=> $message
        )

    );

    wp_insert_post($data);
  

}



add_action( 'init', 'contact_us' );
/**
 * Register a contact post type.
 *
 */

function contact_us(){
    register_post_type( 'contacts',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'Contact Form' ),
                'singular_name' => __( 'Contact' )
            ),
            'rewrite' => false,
            'supports' => array( 'title', 'custom-fields'),
            'public' => false,
            'hierarchical'        => false,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => false,
            'show_in_admin_bar'   => true,
            'menu_position'       => 15,
            'can_export'          => true,
            'has_archive'         => false,
            'exclude_from_search' => true,
            'publicly_queryable'  => false,
            'capability_type'     => 'page',
        )
    );
}




 add_action( 'init', 'news' );
// /**
//  * Register a contact post type.
//  *
//  */

function news(){

register_post_type( 'news',
    // CPT Options
        array(
            'labels' => array(
                'name' => __( 'News' ), 
                'singular_name' => __( 'News' )
            ),
            'rewrite' => array('slug' => 'news-details'),
            'supports' => array('excerpt'),
            'public' => true,
            'hierarchical'        => false,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'can_export'          => true,
            'has_archive'         => false,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'page',
        )
    );  
     
}



function pagination_news() {
    global $wp_query;
 
    if ( $wp_query->max_num_pages > 1 ) { ?>
        <nav class="pagination" role="navigation">
            <div class="nav-previous"><?php next_posts_link( '&larr; previous  ',true ); ?></div>
            <div class="nav-next"><?php previous_posts_link( '&nbsp; Next &rarr;' ,true); ?></div>
        </nav>
<?php }
 wp_reset_postdata();
}
