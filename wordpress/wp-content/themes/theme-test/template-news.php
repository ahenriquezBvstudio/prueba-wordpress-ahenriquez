<?php
/**
* Template Name: Test theme
*
* @package WordPress
* @subpackage First Exercise Acf
*/
get_header();

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$wp_query = new WP_Query(array(

    'posts_per_page' => 4,
    'paged' => $paged,
    'post_type' => 'news',
    'post_status'=> 'publish'

));

?>


<div class="container">
    <div class="row">

        <div class="col-12">
            <h1 class="text-center"> News !!</h1>
        </div>
    </div>

    <div class="row">
        <?php if($wp_query->have_posts()):?>
        <div class="col-12">
            <?php while($wp_query->have_posts()):
                    $wp_query-> the_post();    
                ?>
                    <h5><?php the_field('subtitle')?></h5>

                <div class="media">
                <?php
                            
                            $image = get_field('banner_image');
                            if( $image ):

                            // Image variables.
                            $url = $image['url'];
                            $title = $image['title'];
                            $alt = $image['alt'];
                            $caption = $image['caption'];

                            // Thumbnail size attributes.
                            $size = 'medium';
                            $thumb = $image['sizes'][ $size ];
                            $width = $image['sizes'][ $size . '-width' ];
                            $height = $image['sizes'][ $size . '-height' ];

                            // Begin caption wrap.
                            if( $caption ): ?>
                                <div class="wp-caption">
                                    <?php endif; ?>

                                    <img src="<?php echo esc_url($thumb); ?>" class="card-img-top" alt="<?php echo esc_attr($alt); ?>" />
                                </div>

                        <?php endif; ?>
                <div class="media-body">
                   
                    <?php the_excerpt();?>
                    <a href="<?php the_permalink();?>" class="mb-4">More</a>
                </div>
                </div>
            <?php endwhile;?>
        </div>
     <?php else:?>

     <?php endif;?>
     <?php wp_reset_postdata();?>

    </div>
    <div class="row">

    <?php
        if(function_exists("pagination_news")){
            pagination_news($wp_query->max_num_pages);
        } 
    ?>
    </div>




</div>