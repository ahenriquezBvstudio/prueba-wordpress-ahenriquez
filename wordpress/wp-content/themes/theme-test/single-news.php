<?php

get_header();

?>


<div class="container">
    
    <div class="row">
        
        <div class="col-12">
            <h1> <?php get_field('subtitle') ?></h1>
        </div>

    </div>

    <?php if(have_posts(): while (have_posts()): the_post())?>

    <div class="row">
        
            <div class="col-12">
                
                <div class="media">
                
                    <?php
                            $image = get_field('banner_image');
                            if( $image ):

                            // Image variables.
                            $url = $image['url'];
                            $title = $image['title'];
                            $alt = $image['alt'];
                            $caption = $image['caption'];

                            // Thumbnail size attributes.
                            $size = 'medium';
                            $thumb = $image['sizes'][ $size ];
                            $width = $image['sizes'][ $size . '-width' ];
                            $height = $image['sizes'][ $size . '-height' ];

                            // Begin caption wrap.
                            if( $caption ): ?>
                                <div class="wp-caption">
                                    <?php endif; ?>

                                    <img src="<?php echo esc_url($thumb); ?>" class="card-img-top" alt="<?php echo esc_attr($alt); ?>" />
                                </div>

                        <?php endif; ?>
                    <div class="media-body">
                        <h5> <?php the_field('subtitle') ?></h5>
                             <?php the_field('description') ?>
                    </div>
                </div>

            </div>

    </div>
    <?php endwhile; ?>                          
    <?php endif; ?>


</div>