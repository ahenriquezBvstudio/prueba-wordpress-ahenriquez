let firstname = document.getElementById("first_name");
let lastname = document.getElementById("last_name");
let email = document.getElementById("email");
let message =  document.getElementById("message");


//clean input
function cleanInput() {
    let firstname = document.getElementById("first_name");
    let lastname = document.getElementById("last_name");
    let email = document.getElementById("email");
    let message =  document.getElementById("message");


    firstname.value = "";
    lastname.value = "";
    email.value = "";
    message.value = "";
  
    
}

// valid email regex
function emailIsValid(email) {
    return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
}


// valid field firsname is null
function firstnameIsNullValid() {

    var errorSpan = document.getElementById("formErrorFirstName");
    errorSpan.innerHTML = " is required" // plain javascript
    if (email != "") {
        var errorSpan2 = document.getElementById("formErrorEmail");
        errorSpan2.innerHTML = " " // plain javascript
    }
}
// valid field email is null
function emailIsNullValid() {

    var errorSpan2 = document.getElementById("formErrorEmail");
    errorSpan2.innerHTML = " is required " // plain javascript
    if (firstname != "") {
        var errorSpan = document.getElementById("formErrorFirstName");
        errorSpan.innerHTML = " "
    }
}
// valid field email is not valid
function emailIsNotValid() {

    var errorSpan2 = document.getElementById("formErrorEmail");
    errorSpan2.innerHTML = " email  is not valid"

    if (firstname != "") {
        var errorSpan = document.getElementById("formErrorFirstName");
        errorSpan.innerHTML = " "
    }

}


// clean messages error
function cleanAll() {
    var errorSpan = document.getElementById("formErrorFirstName");
    errorSpan.innerHTML = ""
    var errorSpan2 = document.getElementById("formErrorEmail");
    errorSpan2.innerHTML = ""

}

// show message success
function succesMessage(msg) {
    cleanAll();
    cleanInput();
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    Toast.fire({
        icon: 'success',
        title: msg
    })
}


jQuery(function ($) {

    $('#btn-submit').on('click', function (e) {
        e.preventDefault();
        firstname = $('#first_name').val();
        lastname = $('#last_name').val();
        email = $('#email').val();
        message = $('#message').val();
        nonce = $('#contact_nonce').val();




        if (firstname != "" && email != "" && emailIsValid(email)) {
            //   $("#butsave").attr("disabled", "disabled");
            succesMessage('Record Created');
            $.ajax({
                url: contact_vars.ajaxurl,
                type: "POST",
                data: {
                    action:"contact_form",
                    nonce:nonce,
                    first_name: firstname,
                    last_name: lastname,
                    email: email,
                    message: message
                },
                success: function (dataResult) {
                    console.log('result', dataResult);
                    var dataResult = JSON.parse(dataResult);

                    if (dataResult.statusCode == 200) {
                        //window.location.replace("http://localhost:8000/datatable");
                    }
                    else if (dataResult.statusCode == 404) {
                        alert("Error occured !");
                    }
                }
            });
        }
        else if (firstname == "") {
            firstnameIsNullValid();
        } else if (email == "") {
            emailIsNullValid();
        } else if (!emailIsValid(email)) {
            emailIsNotValid();
        }
    });




});

