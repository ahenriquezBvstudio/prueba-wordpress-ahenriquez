#!/usr/bin/env bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

ODIR=$(pwd)

cd $DIR/wordpress

#php -S 0.0.0.0:80 -t $DIR/wordpress  $DIR/wordpress/local-index.php
#php -S 0.0.0.0:80 routing.php
php -S 0.0.0.0:8080 

cd $ODIR
